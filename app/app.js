(function () {
    'use strict';
    var app = angular.module('app', []);
    app.controller('C1', function ($http){
        var vm = this;
        this.myVar = 1;
        this.newItem = "";
        this.items = [];

        function getItems(){
            $http.get('api/tasks').then(function (result) {
                vm.items = result.data;
            });
        }
        getItems();


        this.increase = function (){
            this.myVar++;
        }
        this.addItem = function (){
            /*var time = new Date();
            var currentTime = time.getHours() + ":"
                + time.getMinutes() + ":"
                + time.getSeconds();
            var itemObject = {title: this.newItem, done: false, time: currentTime};
            this.items.push(itemObject);*/

            var newItem = {
                title: vm.newItem,
                added: new Date()
            };

            $http.post('api/tasks', newItem).then(function () {
                getItems()
                // tehke uus get päring, et vaade uueneks.
            });
        }
    });

})();

